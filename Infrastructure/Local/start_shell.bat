echo off
rem REM Get the full qualified path for the first argument
SET fullpath=%~f1

rem REM Get the drive letter and convert it to lowercase
SET drive=%fullpath:~0,1%
FOR %%i IN ("A=a" "B=b" "C=c" "D=d" "E=e" "F=f" "G=g" "H=h" "I=i" "J=j" "K=k" "L=l" "M=m" "N=n" "O=o" "P=p" "Q=q" "R=r" "S=s" "T=t" "U=u" "V=v" "W=w" "X=x" "Y=y" "Z=z") DO CALL SET drive=%%drive:%%~i%%

rem REM Replace \ with /
SET relpath=%fullpath:~3%
SET relpath=%relpath:\=/%

C:\Windows\System32\bash.exe --login "/mnt/%drive%/%relpath%"