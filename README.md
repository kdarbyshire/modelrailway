Model Railway!
=======
                           .---- -  -
                          (   ,----- - -   TOOT TOOT
                           \_/      ___
                         c--U---^--'o  [_     
                         |------------'_|   
                        /_(o)(o)--(o)(o)
   
 ---

## Contents

This repo contains:

  * the code for the Arduinos in [**LayoutSensorInterface/PingRelay**](#pingrelay);
  * the code to translate pings into GPS locations, and send them to TBUs in [**LayoutSensorInterface/LocationPingPackager**](#locationpingpackager);
  * scripts to interface with JMRI to control the Turnouts in [**LayoutController**](#layoutcontroller) (_not implemented yet [TODO]_);
  * [**Infrastructure**](#infrastructure) - which has structure to copy to Local and Remote hosts, see the Infrastructure section for more information.

It is recommended that \*.pyc is added to your .gitignore

For a rough LOC over-estimate, run:

```
git ls-files | xargs wc -l
```
 
---

## LocationPingPackager

This is a python program that has concurrent threads for:
  * receiving pin/sensor number from the arduinos by serial ports;
  * translating the pin/sensor number into a GPS update message, tracking sensor state and train position;
  * sending the packaged GPS updates to the relevant TBU address.

### Required Packages

  * pySerial - "_pip install pySerial_"

### Code Formatting

  * [PyYapf](https://github.com/jason-kane/PyYapf) used for formatting Python code from within Sublime Text

The non-default Sublime package settings:

```
{
   "yapf_command": "C:\\Python27\\Scripts\\yapf.exe",
   "config" : {
      "COLUMN_LIMIT": 79,
      "CONTINUATION_INDENT_WIDTH": 3,
      "INDENT_WIDTH": 3,
   }
}
```

---

## PingRelay

This Arduino code is to monitor the reed switches and translate the signals into
a known numerical reference (see the sensor map in COMPASS SVN -
physical_architecure S.P1660.250.001) so that a location can be generated that
is representitive of the physical model train position on the HMI trackplans.

  * Debounces digital input monitoring.

[Blog that does what we want](https://arduino.stackexchange.com/questions/19835/arduinomega-64-digital-inputs-cause-random-digitalread-values)

**Dependancies**

  * Adruino IDE or Arduino Create Agaent to use the web IDE.

---

## LayoutController

_Not implemented yet._

When one starts using the DCC-PC interface then keep src under a new dir and 
document README here.

---

## Infrastructure

### Whats here

**Remote**

  * The bash scripts to set up a watchdog on the TBU and allow remote reset.
  * Synchronisation script to copy over the relevent files to the TBU device(s).

**Local**

  * _pi_vnc_ starts a remote control/viewer of the TBU host @ 192.168.1.50. Modify the IP and port i this plaintext file accordingly, or start a new _TightVNC viewer_ session through the application.

### What to do

**COMPASS files**

  * Place the compiled TBU jar in the _Remote_ dir.
  * Place the contents of TBU/Resources in the _Remote/Resources_ dir.
  * Place the contents of Common/lib in the _Remote/lib_ dir.

  * Place the compiled SE and CCE jars in the _Local_ dir.
  * Place the contents of SE/Rsources and CCE/Resources in _Local/SE/Resources_ and _Local/CCE/Resources_ dirs respectively.

**Dependancies**

  * TightVNC

---

## To Run the Basic DMWS Demo

Assuming all the assets are loaded and networked.

Easy but flaky:

  * Start the demo with the batch script **_Infrastructure/Local/start_demo.bat_**.
  * Once everything has started, set the points and signals by loading the
    script for the dmeo scenario desired.
  * Set up the DMWS zone manually from the Control Centre HMI.

Manual:

  * 

---
