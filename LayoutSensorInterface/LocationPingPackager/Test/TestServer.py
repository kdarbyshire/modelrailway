""" Not really meant as a test - just put here so I remember how to make a test
    server.
"""

#LOCAL

import socket as so
import time

while True:
   try:
      s=so.socket(so.AF_INET, so.SOCK_STREAM)
      sa=(('localhost', 10000))
      s.bind(sa)
      s.listen(1)
      print "Trying to accept a new connection"
      con, cli = s.accept()
      running = True
      while running:
         data = con.recv(1024)
         if data != '':
            print data
            print
            time.sleep(0.1)
         if data == 'CLOSE':
            time.sleep(1)
            s.close()
            running = False
   except:
      s.close()




# NETWORK

# import socket as so

# s=so.socket(so.AF_INET, so.SOCK_STREAM)
# sa=(('192.168.1.50', 10000))
# s.bind(sa)
# s.listen(1)
# con, cli = s.accept()
# data = con.recv(1000)

