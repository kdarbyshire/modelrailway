#!/usr/bin/python2
#------------------------------------------------------------------------------
class TestSerialInputs():
   """
   """

   def __init__(self, queue, scenario="Up Oxford"):
      """
      """
      self.queue = queue
      if scenario == "Up Oxford":
         self.up_ox_sim()

   def up_ox_sim(self):
      """ There is a section of track that has 4 sensors that are occupied by
          one train. We simulate this here.
      """
      # self.queue.put("1")
      for i in range(0, 4):
         self.queue.put("%s" % i)
         self.queue.put("%s" % i)
      self.queue.put("1")
      for i in range(4, 7):
         self.queue.put("%s" % i)
      self.queue.put("7")
      for i in range(4, 6):
         self.queue.put("%s" % i)
      for i in range(6, 13):
         self.queue.put("%s" % i)
         self.queue.put("%s" % i)
      self.queue.put("20")
      self.queue.put("20")
      self.queue.put("1")
      self.queue.put("45")
      self.queue.put("7")
      self.queue.put("45")
      self.queue.put("17")
      self.queue.put("17")

   def up_ox_sim_messy(self):
      """ There is a section of track that has 4 sensors that are occupied by
          one train. We simulate this here. There is also a possibility that
          there is a third ping from sensor 1 that would sometimes mess up the
          train location, this is also simulated so that the state stepping can
          be refined to cope.
      """
      for i in range(1, 4):
         self.queue.put("%s" % i)
         self.queue.put("%s" % i)
      for i in range(4, 7):
         self.queue.put("%s" % i)
      for i in range(4, 6):
         self.queue.put("%s" % i)
      self.queue.put("1")
      # Put the extra ping from sensor 1 here.
      for i in range(6, 14):
         self.queue.put("%s" % i)
         self.queue.put("%s" % i)