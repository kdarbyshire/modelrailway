#!/usr/bin/python2
"""   
"""

import threading
import socket
import time
import serial

#------------------------------------------------------------------------------


class SerialReader(threading.Thread):
   def __init__(self, print_lock, queue, sport):
      threading.Thread.__init__(self)
      self.print_lock = print_lock
      self.queue = queue
      self.sport = sport
      self.running = True

   def run(self):
      """
      """
      with self.print_lock: print "| Waiting for data on serial port: %s" % (self.sport)
      while self.running:
         # Wait for data
         data = self.sport.read()

         # Put data on queue
         if data != '':
            self.queue.put(ord(data))
            with self.print_lock:
               print "| Received ping at %s from pin %s\n" % (time.strftime('%X'),
                                                                ord(data))

   def stop(self):
      """
      """
      self.running = False
      with self.print_lock:
         print "| Closing connection to port: %s" % (self.sport)

#------------------------------------------------------------------------------

