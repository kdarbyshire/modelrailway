#!/usr/bin/python2
#------------------------------------------------------------------------------
""" 

"""
#------------------------------------------------------------------------------
from datetime import datetime

from Config import SensorMap
from Config import CommsParams

#------------------------------------------------------------------------------


class SensorState():
   Empty = "Empty"
   Occupied = "Occupied"
   WasOccupied = "WasOccupied"


class SensorStateTable():
   def __init__(self, print_lock):
      self.print_lock = print_lock
      self.sensor_map = SensorMap().get_instance()
      self.state_table = {}
      self.train_positions = {}
      self.start_sensors = []
      self.start_sensor_is_init = False
      scenario_setup = CommsParams.SCENARIO_SETUP[CommsParams.SCENARIO]
      self.init_table(CommsParams.NUM_CLIENTS, scenario_setup)

   def init_table(self, tbu_count, scenario_setup):
      """ If theres 1 model train then initialise the table with the train at
          the position defined for the CommsParams.SCENARIO.
          There are only currently only 2 scenarios - Up Oxford and Up Main.
      """
      for i in range(1, tbu_count + 1):
         for sensor in self.sensor_map:
            self.state_table[sensor] = {}
            if sensor == scenario_setup["Start Position %s" % i]:
               self.start_sensors.append(sensor)
               with self.print_lock:
                  print "| Start position: ", sensor
               self.state_table[sensor]["State"] = SensorState.Occupied
               self.train_positions[i] = {
                  "Sensor": sensor,
                  "Timestamp": datetime.now()
               }
            else:
               self.state_table[sensor]["State"] = SensorState.Empty
            # For the simple test case we have sequential numbering an
            # positions. Sensors in range 1-9 inclusive.
            # if sensor == 1:
            #    self.state_table[sensor]["Adjacent"] = [sensor + 1]
            # elif sensor == len(self.sensor_map):
            #    self.state_table[sensor]["Adjacent"] = [sensor - 1]
            # else:
            #    self.state_table[sensor]["Adjacent"] = [sensor - 1, sensor + 1]

            # For the real case we need to manually input the adjacent sensor
            # nnumbers to ensure they are correct.
            self.state_table[sensor]["Adjacent"] = self.sensor_map[sensor][
               "Adjacent"]

   def step(self, pinged_sensor):
      """ When a sensor on the layout is pinged, we must update the state table
          to ensure we track the front and rear of the trains.
          We identify each train by checking adjacent positions to the pinged
          sensor position.
      """
      if (self.state_table[pinged_sensor]["State"] == SensorState.Empty):
         # Check if there is this is really a new occupation.
         forwardStatesEmpty = True
         for sensor in self.state_table[pinged_sensor]["Adjacent"]["Forward"]:
            if self.state_table[sensor]["State"] != SensorState.Empty:
               forwardStatesEmpty = False
               break;
         backwardStatesEmpty = True
         for sensor in self.state_table[pinged_sensor]["Adjacent"]["Backward"]:
            if self.state_table[sensor]["State"] != SensorState.Empty:
               backwardStatesEmpty = False
               break;
         # for adj in self.state_table[pinged_sensor]["Adjacent"]:
         #    if self.state_table[adj]["State"] != SensorState.Empty:
         if forwardStatesEmpty or backwardStatesEmpty:
            # Shift the "Occupied" status to the most recently pinged.
            self.state_table[pinged_sensor]["State"] = SensorState.Occupied
            # Update the train_location table. It will always be recorded front of
            # the train.
            for train in self.train_positions:
               if self.train_positions[train] in self.state_table[pinged_sensor]["Adjacent"]["Forward"] or \
                self.train_positions[train] in self.state_table[pinged_sensor]["Adjacent"]["Backward"]:
                  self.train_positions[train] = {
                     "Sensor": pinged_sensor,
                     "Timestamp": datetime.now()
                  }
                  break
            # break

      elif (self.state_table[pinged_sensor]["State"] == SensorState.Occupied):
         # if pinged_sensor in self.start_sensors and not self.start_sensor_is_init:
         #    self.start_sensor_is_init = True
         #    return
         # The sensor was "Occupied" so the ping was the read of the train
         # leaving (or the train reversing and the front of the train
         # crossing the sensor and also leaving it "WasOccupied").
         self.state_table[pinged_sensor]["State"] = SensorState.WasOccupied
         # Also if any of the adjacent sensors to this unoccupied sensor
         # were "WasOccupied".
         for dir in self.state_table[pinged_sensor]["Adjacent"]: 
           for adj in self.state_table[pinged_sensor]["Adjacent"][dir]:
              if adj in self.state_table:
                 if self.state_table[adj]["State"] == SensorState.WasOccupied:
                    self.state_table[adj]["State"] = SensorState.Empty

      if CommsParams.DEBUG_MODE:
         with self.print_lock:
            self.print_state()

   def get_train_number_from_sensor(self, sensor):
      """ As we know the start positions of each train we can track as they
          move to adjacent points. This method will return an int representing
          the train is at the input sensor number, or None if there are no
          trains there.
      """
      # If there is on train just return the sensor position.
      if len(self.train_positions) == 1:
         return 1
      for train in self.train_positions:
         if self.train_positions[train]["Sensor"] == sensor:
            return train
      return None

   def is_new_occupation(self, sensor):
      """ For received pings, THIS METHOD IS CALLED AFTER step() and will
          indicate whether the sensor state is newly occupied.

          Random pings on the track will set this off as if its a new occupation
          regardless of whether its sensible. Things we could do are listed here
          but all have error cases that are not satisfactory.

          1.
          Rules for new Occupation:
          - The sensor must be adjacent to an sensor that is not Empty.

          This means that if there is a faulty reed and a sensor is missed
          then the new occupation will not be recorded...

          2.
          Allow one point of separation to a non Empty sensor?

          This will not cause much of a jump on the track 
      """
      if self.state_table[sensor]["State"] == SensorState.Occupied:
         return True
      else:
         return False

   def print_state(self):
      """ Print a friedly view of the state table.
          Ignore Empty sensors to declutter.
      """
      print
      for sensor in self.state_table:
         if self.state_table[sensor]["State"] != SensorState.Empty:
            print sensor, "\t", self.state_table[sensor]["State"]
      print
